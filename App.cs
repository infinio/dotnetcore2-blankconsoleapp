﻿using System.Threading.Tasks;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ConsoleApp
{
    public class App
    {
        private ILogger<App> Logger { get; }
        private IConfigurationRoot Config { get; }

        public App(IConfigurationRoot config, ILogger<App> logger)
        {
            Logger = logger;
            Config = config;
            
        }

        public async Task Run(string[] args)
        {
            var app = new CommandLineApplication { Name = "ConsoleApp" };
            app.HelpOption("-?|-h|--help");

            app.OnExecute(async () =>
            {
                
                return 0;
            });
            

            app.Execute(args);
        }

    }
}
