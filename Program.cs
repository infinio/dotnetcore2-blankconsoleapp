﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;


namespace ConsoleApp
{
    class Program
    {
        private static readonly string LOG_FILE_PATH = "Logs/log-{Date}.txt";
        private static string _baseDir;

        static void Main(string[] args)
        {
            try
            {
                _baseDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                MainAsync(args).Wait();
            }
            catch (Exception e)
            {
                Log.Logger.Fatal(e, string.Empty);
            }
        }


        static async Task MainAsync(string[] args)
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // Create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();


            await serviceProvider.GetService<App>().Run(args);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Add logging
            serviceCollection.AddSingleton(new LoggerFactory().AddSerilog().AddDebug());
            serviceCollection.AddLogging();

            // Build configuration
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            // Initialize serilog logger
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                .WriteTo.RollingFile(_baseDir + "/" + LOG_FILE_PATH, outputTemplate: "[{SourceContext}][{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
                .CreateLogger();

            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton(config);

            serviceCollection.AddTransient<App>();
        }
    }
}
